package com.example.demo;

import com.example.demo.dao.CustomerDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MyCommandLineRunner implements CommandLineRunner {
    @Autowired
    CustomerDAO customerDAO;
    @Override
    public void run(String... args) throws Exception {
        customerDAO.getCustomers().forEach(System.out::println);
    }
}
