package com.example.demo.dao;

import com.example.demo.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class CustomerDAO {

@Autowired
JdbcTemplate jdbcTemplate;

    public List<Customer> getCustomers() throws Exception{
        List<Customer>customers= jdbcTemplate.query("Select * from customer", new RowMapper<Customer>() {
            @Override
            public Customer mapRow(ResultSet resultSet, int i) throws SQLException {
                int id =resultSet.getInt("id");
                String name =resultSet.getString("name");
                String address =resultSet.getString("address");

                return new Customer(id,name,address);
            }
        });
        return customers;
    }



}
